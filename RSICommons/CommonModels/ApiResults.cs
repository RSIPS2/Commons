﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSICommons.CommonModels
{
    public class ApiResult
    {
        public bool status { get; set; }
        public object result { get; set; }
        public ApiResult() { }
        public ApiResult(bool status, object result)
        {
            this.status = status;
            this.result = result;
        }
        public ApiResult(Tuple<bool, object> r)
        {
            status = r.Item1;
            result = r.Item2;
        }
    }
}
