﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSICommons.CommonModels.DTO.Enums
{
    public enum DTOTypes
    {
        Default = 0,
        Person = 1,
        Product = 2,
        Subscription = 3,
        PersonProduct = 4,
    }

    public interface IDTOType
    {
        Guid Id { get; set; }
    }   
}
