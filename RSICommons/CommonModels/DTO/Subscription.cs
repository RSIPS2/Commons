﻿using RSICommons.CommonModels.DTO.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSICommons.CommonModels.DTO
{
    public class Subscription : IDTOType
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid ProductId { get; set; }
        public SubscriptionType Type { get; set; }

        public virtual Product Product { get; set; }

        internal static DTOTypes GetObjectType() => DTOTypes.Subscription;
    }
}
