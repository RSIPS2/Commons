﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace RSICommons.Helpers
{
    public static class EmailService
    {
        public static async Task<EmailStatus> SendMail(MailViewModel message)
        {
            try
            {
                var msg = new MailMessage("2abe1639f0-2d90a2@inbox.mailtrap.io", message.Mail);
                msg.Body = message.Content;
                msg.Subject = message.Title + " " + message.Name + " " + message.Surname;
                msg.From = new MailAddress("2abe1639f0-2d90a2@inbox.mailtrap.io");
                msg.ReplyToList.Add(new MailAddress(message.Mail));


                using (var smtp = new SmtpClient())
                {
                    smtp.Credentials = new System.Net.NetworkCredential("6dfcbdbbc97114", "657bc36d965be8");
                    smtp.Host = "smtp.mailtrap.io";
                    smtp.Port = 465;
                    smtp.EnableSsl = true;
                    await smtp.SendMailAsync(msg);
                }

                return EmailStatus.Sent;

            }
            catch (Exception ex)
            {
                return EmailStatus.Error;
            }
        }
    }

    public class MailViewModel
    {
        public string Content { get; set; }
        public string Mail { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Title { get; set; }
    }

    public enum EmailStatus
    {
        Sent,
        Error
    }
}
