﻿using RSICommons.CommonModels.DTO.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSICommons.CommonModels.DTO
{
    public class PersonProduct : IDTOType
    {
        public Guid Id { get; set; }

        public DateTime BuyTime { get; set; }
        public int Amount { get; set; }
        public string Price { get; set; }

        public Guid PersonId { get; set; }
        public Guid ProductId { get; set; }

        public Person Person { get; set; }
        public Product Product { get; set; }

        internal static DTOTypes GetObjectType() => DTOTypes.PersonProduct;
    }
}
