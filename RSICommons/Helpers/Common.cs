﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSICommons.Helpers
{
    public static class Common
    {
        public static class Apis
        {
            /// <summary>
            /// Gets items of chosen type
            /// <para>parameters: /type/count/skip/selectByUserOrProductID/selectOnlyIdsString</para> 
            /// <para>last 2 are optional</para> 
            /// </summary>
            public const string GetItemsByType = "/Api/GetItems";

            /// <summary>
            /// Gets item of chosen type by its ID
            /// <para>parameters: /type/id</para> 
            /// </summary>
            public const string GetItemById = "/Api/GetItemById";

            /// <summary>
            /// Search for Books
            /// <para>parameters: /query/count/skip</para> 
            /// <para>last 2 are optional</para> 
            /// </summary>
            public const string FindProducts = "/Api/FindProducts";

            /// <summary>
            /// Deletes objects by IDS
            /// <para>parameters: /type/ids</para> 
            /// </summary>
            public const string Delete = "/Api/Delete";

        }
    }
}
