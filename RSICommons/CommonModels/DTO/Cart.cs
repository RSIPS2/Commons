﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSICommons.CommonModels.DTO
{
    public class Cart
    {
        public Guid ProductId { get; set; }
        public int Count { get; set; }
    }
}
