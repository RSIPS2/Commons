﻿using RSICommons.CommonModels.DTO.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSICommons.CommonModels.DTO
{
    public class Product : IDTOType
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Description { get; set; }
        public string Publisher { get; set; }
        public string PublishDate { get; set; }
        public int PageCount { get; set; }
        public string Price { get; set; }
        public string Genre { get; set; }
        public string Language { get; set; }
        public string OriginalLanguage { get; set; }
        public string Thumbnail { get; set; }
        public string TranslatedBy { get; set; }
        public int Amount { get; set; }

        public List<PersonProduct> PersonProducts { get; set; }
        public List<Subscription> Subscriptions { get; set; }

        internal static DTOTypes GetObjectType() => DTOTypes.Product;
    }
}
