﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RSICommons.CommonModels;
using RSICommons.CommonModels.DTO;
using RSICommons.CommonModels.DTO.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RSICommons.Helpers
{
    public static class ApiHelper
    {
        public static async Task<IEnumerable<IDTOType>> GetItemsFromApi<IDTOType>(string authorize,  string host, IDTOType defaultValue, params string[] parameters) where IDTOType : class
        {
            List<IDTOType> outputObject = new List<IDTOType>();
            outputObject.Add(defaultValue);
            try
            {
                if (string.IsNullOrEmpty(host))
                    return outputObject;

                var output = await ApiHelper.Get(authorize, host, Common.Apis.GetItemsByType, parameters);
                if (output.IsSuccessStatusCode)
                {
                    return await output.Content.ReadAsAsync<List<IDTOType>>() ?? outputObject;
                    //outputObject = JsonConvert.DeserializeObject<List<IDTOType>>(await output.Content.ReadAsStringAsync()) ?? outputObject;
                    //return outputObject;
                }
                else
                {
                    return null;
                }
            }
            catch (WebException wex)
            {
                var pageContent = new StreamReader(wex.Response.GetResponseStream())
                                      .ReadToEnd();
            }
            catch (Exception ex)
            {

            }

            return outputObject;
        }
        public static async Task<HttpResponseMessage> Get(string authorize, string host, string method, params string[] parameters)
        {
            try
            {
                var uri = $"{host}{method}{CreateParametersString(parameters)}";
                HttpClient client = new HttpClient();
                client.CancelPendingRequests();
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(uri),
                    Method = HttpMethod.Get,
                    //Content = new StringContent("", Encoding.UTF8, "application/json")    
                };
                request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authorize);

                var output = await client.SendAsync(request).ConfigureAwait(false);

                if (output.StatusCode == HttpStatusCode.RequestTimeout && host.Equals("https://crud-rsi.azurewebsites.net"))
                {
                    TokenHelper.SaveTokenToFile(TokenHelper.DefaultToken);
                }

                IEnumerable<string> values;
                if (output.Headers.TryGetValues("Auth", out values) && host.Equals("https://crud-rsi.azurewebsites.net"))
                {
                    string auth = values.FirstOrDefault();
                    TokenHelper.SaveTokenToFile(auth);
                }
                return output;
            }
            catch (WebException wex)
            {
                TokenHelper.SaveTokenToFile(TokenHelper.DefaultToken);
                var pageContent = new StreamReader(wex.Response.GetResponseStream())
                                      .ReadToEnd();
            }
            catch (Exception ex)
            {
                TokenHelper.SaveTokenToFile(TokenHelper.DefaultToken);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }


        public static async Task<ApiResult> PostToApi<T>(string authorize, string host, string method, string type, T dataObjecet) where T : class
        {
            string status = "";
            HttpResponseMessage response = null;
            try
            {
                var uri = $"{host}{method}/{type}";
                HttpClient client = new HttpClient();
                client.CancelPendingRequests();
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(uri),
                    Method = HttpMethod.Post,
                    Content = new StringContent(JsonConvert.SerializeObject(dataObjecet), Encoding.UTF8, "application/json")    
                };
                request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authorize);

                response = (await client.SendAsync(request).ConfigureAwait(false));

                if (response.StatusCode == HttpStatusCode.RequestTimeout && host.Equals("https://crud-rsi.azurewebsites.net"))
                {
                    TokenHelper.SaveTokenToFile(TokenHelper.DefaultToken);
                }

                IEnumerable<string> values;
                if (response.Headers.TryGetValues("Auth", out values) && host.Equals("https://crud-rsi.azurewebsites.net"))
                {
                    string auth = values.FirstOrDefault();
                    TokenHelper.SaveTokenToFile(auth);
                }

                if (response.StatusCode == HttpStatusCode.OK)
                    return new ApiResult(true, response.RequestMessage) ;

                status = response.StatusCode.ToString();


                //var httpWebRequest = (HttpWebRequest)WebRequest.Create(host + method + "/" + type);
                //httpWebRequest.Method = "POST";
                //httpWebRequest.ContentType = "application/json";
                //using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                //{

                //    string json = JsonConvert.SerializeObject(dataObjecet);

                //    streamWriter.Write(json);
                //    streamWriter.Flush();
                //    streamWriter.Close();

                //    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                //    if (httpResponse.StatusCode == HttpStatusCode.OK)
                //        return new ApiResult(true, "Completed");
                //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                //{
                //    var result = streamReader.ReadToEnd();
                //    if (string.IsNullOrEmpty(result))
                //        return new ApiResult(true, "Completed");
                //}
                //}
            }
            catch (WebException wex)
            {
                var pageContent = new StreamReader(wex.Response.GetResponseStream())
                                      .ReadToEnd();
                TokenHelper.SaveTokenToFile(TokenHelper.DefaultToken);
            }
            catch (Exception ex)
            {
                return new ApiResult(false, "Unknown Error " + status);
                TokenHelper.SaveTokenToFile(TokenHelper.DefaultToken);
            }

            return new ApiResult(true, "Completed " + status);
        }

        public static async Task<ApiResult> DeleteFromApi(string authorize, string host, string objectType, IEnumerable<Guid> ids)
        {
            var status = "";
            try
            {
                var idsToDelete = ids.Select(id => id.ToString()).Aggregate((idsString, id) => idsString.ToString() + "<==>" + id.ToString());


                var uri = $"{host}{Common.Apis.Delete}/{objectType}/{idsToDelete}";
                HttpClient client = new HttpClient();
                client.CancelPendingRequests();
                var request = new HttpRequestMessage()
                {
                    RequestUri = new Uri(uri),
                    Method = HttpMethod.Delete,
                };
                request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", authorize);

                var response = (await client.SendAsync(request).ConfigureAwait(false));
                if (response.StatusCode == HttpStatusCode.RequestTimeout && host.Equals("https://crud-rsi.azurewebsites.net"))
                {
                    TokenHelper.SaveTokenToFile(TokenHelper.DefaultToken);
                }

                IEnumerable<string> values;
                if (response.Headers.TryGetValues("Auth", out values) && host.Equals("https://crud-rsi.azurewebsites.net"))
                {
                    string auth = values.FirstOrDefault();
                    TokenHelper.SaveTokenToFile(auth);
                }


                if (response.StatusCode == HttpStatusCode.OK)
                    return new ApiResult(true, "Completed");

                status = response.StatusCode.ToString();

                //WebRequest request = WebRequest.Create($"{host}{Common.Apis.Delete}/{objectType}/{idsToDelete}");
                //request.Method = "DELETE";
                //request.Headers.Add(HttpRequestHeader.Authorization, "OcguCWTZBjTIoM5Ol8dZCiFBw4mHsDiTBjWjfq9X=");

                //HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                //if (response.StatusCode == HttpStatusCode.OK)
                //    return new ApiResult(true, "Completed");

                //return new ApiResult(false, response.StatusCode.ToString());
            }
            catch (WebException wex)
            {
                var pageContent = new StreamReader(wex.Response.GetResponseStream())
                                      .ReadToEnd();
                TokenHelper.SaveTokenToFile(TokenHelper.DefaultToken);
            }
            catch (Exception ex)
            {
                TokenHelper.SaveTokenToFile(TokenHelper.DefaultToken);
            }

            return new ApiResult(false, "Error " + status);
        }

        public static async Task<ApiResult> SellProduct(string authorize, string host, Guid userId, IEnumerable<Cart> cartList)
        {
            try
            {
                return await ApiHelper.PostToApi(authorize, host, "/api/SellProduct", userId.ToString(), cartList);
            }
            catch (WebException wex)
            {
                var pageContent = new StreamReader(wex.Response.GetResponseStream())
                                      .ReadToEnd();
                TokenHelper.SaveTokenToFile(TokenHelper.DefaultToken);
            }
            catch (Exception ex)
            {
                TokenHelper.SaveTokenToFile(TokenHelper.DefaultToken);
            }

            return new ApiResult(false, "Unknown Error");
        }

        public static async Task<Guid> GetUserByProviderId(string authorize, string host, string Id, Provider provider)
        {
            try
            {
                var output = await ApiHelper.Get(authorize, host, "/api/GetUserByProviderId", provider.ToString(), Id);

                if (output.IsSuccessStatusCode)
                {
                    var person = await output.Content.ReadAsAsync<Guid>();
                    return person;
                }
                else
                {
                    return Guid.Empty;
                }

            }
            catch (WebException wex)
            {
                var pageContent = new StreamReader(wex.Response.GetResponseStream())
                                      .ReadToEnd();
            }
            catch (Exception ex)
            {

            }

            return Guid.Empty;
        }


        private static string CreateParametersString(string[] parameters)
        {
            if (parameters == null || parameters.Length == 0)
                return null;

            var outputStringBuilder = new StringBuilder();
            foreach (var param in parameters)
            {
                outputStringBuilder.Append("/");
                outputStringBuilder.Append(Uri.EscapeDataString(param));
            }
            outputStringBuilder.Append("/");
            return outputStringBuilder.ToString();
        }
    }
}