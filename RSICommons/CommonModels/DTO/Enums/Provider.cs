﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSICommons.CommonModels.DTO.Enums
{
    public enum Provider
    {
        Default = 0,
        Google = 1,
        Twitter = 2,
        Facebook = 3,
    }
}
