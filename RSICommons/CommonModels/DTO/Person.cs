﻿using Newtonsoft.Json;
using RSICommons.CommonModels.DTO.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSICommons.CommonModels.DTO
{
    public class Person : IDTOType
    {
        public Guid Id { get; set; }
        public string OAuth2IdGoogle { get; set; }
        public string OAuth2IdTwitter { get; set; }
        public string OAuth2IdFacebook { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }

        public List<PersonProduct> PersonProducts { get; set; }
        public List<Subscription> Subscriptions { get; set; }

        public static DTOTypes GetObjectType() => DTOTypes.Person;

    }
}
