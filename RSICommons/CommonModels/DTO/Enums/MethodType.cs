﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSICommons.CommonModels.DTO.Enums
{
    public enum MethodType
    {
        [Description("Client module - Subscribe product by Id")]
        ClientModuleSubscribeProductById = 1,
        [Description("Client module - Subscribe")]
        ClientModuleSubscribe = 2,
        [Description("Client module - Buy product")]
        ClientModuleBuyProduct = 3,
        [Description("Client module - Search")]
        ClientModuleSearch = 4
    }
}
