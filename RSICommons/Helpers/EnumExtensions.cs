﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSICommons.Helpers
{
    public static class EnumExtensions
    {
        public static T ToEnum<T>(this string value, bool ignoreCase = false) where T : struct, IConvertible
        {
            try
            {
                if (typeof(T).IsEnum && !string.IsNullOrEmpty(value))
                {
                    T result;
                    if (Enum.TryParse<T>(value, ignoreCase, out result))
                        return result;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            return default(T);
        }

        public static string[] Split(this string value, string splitBy)
        {
            return value.Split(new string[] { splitBy }, StringSplitOptions.None);
        }
    }
}
