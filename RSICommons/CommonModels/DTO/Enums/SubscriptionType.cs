﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSICommons.CommonModels.DTO.Enums
{
    public enum SubscriptionType
    {
        Default = -1,
        Product = 0,
        Newsletter = 1
    }
}
