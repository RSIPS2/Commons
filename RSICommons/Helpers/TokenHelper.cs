﻿using MlkPwgen;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSICommons.Helpers
{
    public static class TokenHelper
    {
        public static string DefaultToken { get; } = "OcguCWTZBjTIoM5Ol8dZCiFBw4mHsDiTBjWjfq9X=";
        public static void SaveTokenToFile(string key, string path = null)
        {
            path = @"mykey";
            var obj = JsonConvert.SerializeObject(key);
            File.WriteAllText(path, obj);
        }

        public static string ReadTokenFromFile(string path = null)
        {
            path = @"mykey";
            if (File.Exists(path))
            {
                using (StreamReader sr = File.OpenText(path))
                {
                    string s = sr.ReadToEnd();
                    return JsonConvert.DeserializeObject<string>(s);
                }
            }
            return null;
        }

        public static void SaveToFile(IEnumerable<string> keys, string path = null)
        {
            path = @"D:\home\site\wwwroot\keys";
            var obj = JsonConvert.SerializeObject(keys);
            while (saveWorkaround(obj, path));
        }

        private static bool saveWorkaround(string obj, string path)
        {
            try
            {
                File.WriteAllText(path, obj);
            }
            catch (IOException)
            {
                return true;
            }
            return false;
        }

        public static List<string> ReadFromFile(string path = null)
        {
            path = @"D:\home\site\wwwroot\keys";
            if (File.Exists(path))
            {
                using (StreamReader sr = File.OpenText(path))
                {
                    string s = sr.ReadToEnd();
                    return JsonConvert.DeserializeObject<List<string>>(s);
                }
            }
            return null;
        }

        public static string GeneratePswd()
        {
            return PasswordGenerator.Generate(length: 30, allowed: Sets.Alphanumerics);
        }
    }
}
