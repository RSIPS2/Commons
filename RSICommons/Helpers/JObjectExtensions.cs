﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace RSICommons.Helpers
{
    public static class JObjectExtensions
    {
        public static T GetValueByKey<T>(this JObject jobject, string key, T defaultValue)
        {
            JToken value = null;
            try
            {
                if (string.IsNullOrEmpty(key) || jobject == null || !jobject.HasValues)
                    return defaultValue;
                if (jobject.TryGetValue(key, out value))
                {
                    if (value == null)
                        return defaultValue;
                }
                else
                    return defaultValue;
                try
                {
                    return (T)Convert.ChangeType(value, typeof(T));
                }
                catch (InvalidCastException)
                {
                    return defaultValue;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            return defaultValue;
        }

        public static T GetValueByKey<T>(this JToken jtoken, string key, T defaultValue)
        {
            T value = default(T);
            try
            {
                if (string.IsNullOrEmpty(key) || jtoken == null || !jtoken.HasValues)
                    return defaultValue;
                try
                {
                    value = jtoken.Value<T>(key);
                    return value != null ? value : defaultValue;
                }
                catch (InvalidCastException)
                {
                    return defaultValue;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            return defaultValue;
        }

        public static JObject SetValueByKey(this JObject jobject, string key, object value)
        {
            try
            {
                if (string.IsNullOrEmpty(key) || jobject == null || !jobject.HasValues)
                    return jobject;
                jobject[key] = JToken.FromObject(value);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            return jobject;
        }
        
        public static T GetValueByPath<T>(this JToken jtoken, IEnumerable<string> path, T defaultValue)
        {
            try
            {
                if (!path.Any() || jtoken == null || !jtoken.HasValues)
                    return defaultValue;
                var currentValue = jtoken;
                foreach (var pathPart in path)
                {
                    if (!currentValue.HasValues || currentValue[pathPart] == null)
                        return defaultValue;

                    currentValue = currentValue[pathPart];
                }
                try
                {
                    return (T)Convert.ChangeType(currentValue, typeof(T));
                }
                catch (InvalidCastException)
                {
                    return defaultValue;
                }
            }
            catch (Exception ex)
            {
                 Debug.WriteLine(ex.Message);
            }
            return defaultValue;
        }
    }
}